//
//  gitlab_ci_pipelineTests.swift
//  gitlab-ci-pipelineTests
//
//  Created by 9357950 on 14/08/2018.
//  Copyright © 2018 James Robert Sawley. All rights reserved.
//

import XCTest
@testable import gitlab_ci_pipeline

class gitlab_ci_pipelineTests: XCTestCase {
    
    var controllerUnderTest: ViewController!
    
    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controllerUnderTest = storyboard.instantiateInitialViewController() as! ViewController
        controllerUnderTest.loadView()
    }
    
    override func tearDown() {
        controllerUnderTest = nil
        super.tearDown()
    }
    
    func test_helloWorldExists() {
        let labelText = controllerUnderTest.helloWorldOutlet.titleLabel!.text
        XCTAssertEqual(labelText, "Hello World")
    }
    
}
