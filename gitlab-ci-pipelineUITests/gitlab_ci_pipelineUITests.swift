//
//  gitlab_ci_pipelineUITests.swift
//  gitlab-ci-pipelineUITests
//
//  Created by 9357950 on 14/08/2018.
//  Copyright © 2018 James Robert Sawley. All rights reserved.
//

import XCTest

class gitlab_ci_pipelineUITests: XCTestCase {
        
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDown() {
        app = nil
        super.tearDown()
    }
    
    func test_whenUserClicksHelloWorldButtonItChangesToFooBar() {
        
        let helloWorld = app.buttons["Hello World"]
        let fooBar = app.buttons["Foo Bar"]
        let exists = NSPredicate(format: "exists == true")
        
        expectation(for: exists, evaluatedWith: fooBar, handler: nil)
        helloWorld.tap()
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssert(fooBar.exists)
        
    }
    
    func test_whenUserClicksFooBarButtonItChangesBackToHelloWorld() {
        
        let helloWorld = app.buttons["Hello World"]
        let fooBar = app.buttons["Foo Bar"]
        let exists = NSPredicate(format: "exists == true")
        
        helloWorld.tap()
        
        expectation(for: exists, evaluatedWith: helloWorld, handler: nil)
        fooBar.tap()
        waitForExpectations(timeout: 2, handler: nil)
        XCTAssert(helloWorld.exists)
        
    }
    
}
