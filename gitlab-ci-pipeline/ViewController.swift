//
//  ViewController.swift
//  gitlab-ci-pipeline
//
//  Created by 9357950 on 14/08/2018.
//  Copyright © 2018 James Robert Sawley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloWorldOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func helloWorldButton(_ sender: Any) {
        var text : String = "Hello World"
        
        if helloWorldOutlet.titleLabel!.text == "Hello World" {
            text = "Foo Bar"
        }
        
        helloWorldOutlet.setTitle(text, for: .normal)
    }
}

